# SPDX-FileCopyrightText: 2023 Galagic Limited, et al. <https://galagic.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Self-hosted lists of any kind: wishlists, task lists, collections and more.
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/listidge/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import uuid
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils.http import url_has_allowed_host_and_scheme
from django.utils.encoding import iri_to_uri

from .models import List, ListGiftItem, ListType, ListToDoItem
from .listmanagers import get_list_mgr


def check_demo_user(username):
    if settings.DEMO_USER and username == 'demo':
        if not User.objects.filter(username="demo").exists():
            User.objects.create_user("demo", 'lennon@thebeatles.com', "demo")


def redirect(request):
    return HttpResponseRedirect(reverse('listidge:lists'))


def login_user(request):
    # Next parameter need careful processing
    # https://stackoverflow.com/a/64154187/28170
    # https://stackoverflow.com/a/60372947/28170
    paramdict = {"registration": settings.REGISTRATION}

    if settings.DEMO_USER:
        paramdict['demo'] = "True"

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        check_demo_user(username)

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            next = request.POST['next']
            if next is None:
                return HttpResponseRedirect(reverse('listidge:lists'))
            elif url_has_allowed_host_and_scheme(
                    url=next,
                    allowed_hosts={request.get_host()},
                    require_https=request.is_secure()):
                return HttpResponseRedirect(iri_to_uri(next))
            else:
                return HttpResponseRedirect(reverse('listidge:lists'))
        else:
            paramdict['error'] = "Login failed"
            return render(request, 'listidge/login.html', paramdict)
    else:
        if 'next' in request.GET:
            paramdict['next'] = request.GET['next']
        return render(request, 'listidge/login.html', paramdict)


def register_user(request):
    if settings.REGISTRATION:
        if request.method == 'POST':
            username = request.POST['username']
            password = request.POST['password']

            if User.objects.filter(username=username).exists():
                return render(request, 'listidge/register.html',
                              {'error': "Username already taken"})

            user = User.objects.create_user(username, 'lennon@thebeatles.com',
                                            password)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect(reverse('listidge:lists'))
            else:
                return HttpResponseRedirect('.')
        else:
            return render(request, 'listidge/register.html')
    else:
        return HttpResponseRedirect('.')


def logout_user(request):
    logout(request)
    return HttpResponseRedirect(reverse('listidge:redirect'))


@login_required
def lists(request):
    if request.method == 'POST':
        lstype = get_object_or_404(ListType, name=request.POST['listype'])
        lst = List(user=request.user,
                   name=request.POST['name'],
                   description=request.POST['description'],
                   listype=lstype)
        lst.save()
        return HttpResponseRedirect('.')
    else:
        lists = List.objects.filter(user=request.user)
        return render(request, 'listidge/lists.html', {'lists': lists})


@login_required
def list(request, list_id):
    lst = List.objects.filter(user=request.user, id=list_id)
    if not lst:
        return render(request, 'listidge/noperm.html')

    lst = get_object_or_404(List, pk=list_id)
    mgr = get_list_mgr(lst)

    if request.method == 'POST':
        (success, errors) = mgr.create_item(request.POST, request.FILES)
        if not success:
            (template, items) = mgr.get_items()
            return render(request, template,
                          {'list': lst, "items": items, "errors": errors})

        return HttpResponseRedirect('.')
    else:
        (template, items) = mgr.get_items()
        return render(request, template,
                      {'list': lst, "items": items})


def list_public(request, list_uuid):
    lst = get_object_or_404(List, uuid=list_uuid)
    mgr = get_list_mgr(lst)
    (template, context) = mgr.get_public_items()
    return render(request, template, context)


@login_required
def list_delete(request, list_id):
    lst = List.objects.filter(user=request.user, id=list_id)
    if not lst:
        return render(request, 'listidge/noperm.html')

    lst = get_object_or_404(List, pk=list_id)
    lst.delete()
    return HttpResponseRedirect(reverse('listidge:lists'))


@login_required
def item_delete(request, list_id, item_id):
    lst = List.objects.filter(user=request.user, id=list_id)
    if not lst:
        return render(request, 'listidge/noperm.html')

    lst = get_object_or_404(List, pk=list_id)
    mgr = get_list_mgr(lst)
    mgr.delete_item(item_id)
    return HttpResponseRedirect(reverse('listidge:list', args=(list_id,)))


@login_required
def list_share_from_list(request, list_id):
    lst = List.objects.filter(user=request.user, id=list_id)
    if not lst:
        return render(request, 'listidge/noperm.html')

    lst = get_object_or_404(List, pk=list_id)
    lst.uuid = uuid.uuid4()
    lst.save()
    return HttpResponseRedirect(reverse('listidge:list', args=(list_id,)))


@login_required
def list_share_from_lists(request, list_id):
    lst = List.objects.filter(user=request.user, id=list_id)
    if not lst:
        return render(request, 'listidge/noperm.html')

    lst = get_object_or_404(List, pk=list_id)
    lst.uuid = uuid.uuid4()
    lst.save()
    return HttpResponseRedirect(reverse('listidge:lists'))


def item_reserve(request, list_uuid, item_id):
    lst = get_object_or_404(List, uuid=list_uuid)
    lgi = get_object_or_404(ListGiftItem, id=item_id)
    lgi.amount -= 1
    lgi.save()
    # Must be a better way to send them back to the public link
    return HttpResponseRedirect(reverse('listidge:list_public',
                                        args=(lst.uuid,)))


def todoitem_update(request, list_uuid, item_id):
    lst = get_object_or_404(List, uuid=list_uuid)
    ltdi = get_object_or_404(ListToDoItem, id=item_id)
    ltdi.state = request.POST['state']
    ltdi.completed = 'completed' in request.POST
    ltdi.save()
    # Must be a better way to send them back to the public link
    return HttpResponseRedirect(reverse('listidge:list_public',
                                        args=(lst.uuid,)))
