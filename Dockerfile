# SPDX-FileCopyrightText: 2023 Galagic Limited, et al. <https://galagic.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Self-hosted lists of any kind: wishlists, task lists, collections and more.
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/listidge/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

FROM tiangolo/uwsgi-nginx:python3.11

COPY requirements.txt /app/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

COPY djangosite /app

# Configure container
# https://github.com/tiangolo/uwsgi-nginx-docker
#
# Restrict upload size for sanity
ENV NGINX_MAX_UPLOAD=5m
# Take advantage of more CPUs if avail
ENV NGINX_WORKER_PROCESSES auto
# Custom nginx config
COPY listidge.conf /etc/nginx/conf.d/listidge.conf
# Our prestart script
COPY prestart.sh /app

# Django settings
ENV ALLOWED_HOSTS=localhost,127.0.0.1

# Prepare static files (only necessary when DEBUG off)
RUN python manage.py collectstatic
